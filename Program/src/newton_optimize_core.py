from scipy import optimize
import numdifftools as nd
import numpy
import math
from math import pi
from math import e

def gradient(fun,xi):
    return nd.Gradient(fun)(xi)

def hessian(fun,xi):
    return nd.Hessian(fun)(xi)

def hessian_inv(fun,xi):
    return numpy.linalg.inv(hessian(fun,xi))

def sin(x):
    return numpy.sin(x)

def cos(x):
    return numpy.cos(x)

def tan(x):
    return numpy.tan(x)

def sqrt(x):
    return numpy.sqrt(x)

def exp(x):
    return numpy.exp(x)

def log(x):
    return numpy.log(x)

def radians(x):
    return math.radians(x)

def newton(function,xi):
    fun = eval("lambda x: " + function)
    xi = list(map(float,xi.split(',')))

    ## initial values
    eps_norm = 1.0
    eps1_norm = 1.0
    eps2_norm = 1.0
    step = 0

    ## algorithm parameters
    eps = 10**(-6)
    eps1 = 10**(-6)
    eps2 = 10**(-6)
    step_max = 50

    step_list=[xi]
    fx_list=[fun(xi)]

    ## algorithm
    g=0.1;
    while eps_norm > eps and eps1_norm > eps1 and eps2_norm > eps2 and step < step_max:
        x = xi
        
        delta_x=numpy.dot(hessian_inv(fun,x),gradient(fun,x)) 
         
        gamma = sqrt(2*g/(numpy.linalg.norm(delta_x,2)))
    
        gamma= min(gamma,1) 
        
        xi = x - gamma*delta_x;
        step = step + 1

        eps1_norm = numpy.linalg.norm(numpy.array(xi)-numpy.array(x))
        eps2_norm = numpy.linalg.norm(fun(xi)-fun(x))
       
        step_list.append(xi)
        fx_list.append(fun(xi))
        print(step,eps1_norm,eps2_norm,gamma)
        
    ## check stop condition
    if eps_norm<=eps:
        stop_cond='1. < grad(f(xi), grad(f(xi)) >  <= ' + str(eps)
    elif eps1_norm<=eps1:
        stop_cond='2. || x(i)- x(i-1) || <= ' + str(eps1)
    elif eps2_norm<=eps2:
        stop_cond='3. | f(x(i)) - f(x(i-1)) | <=' + str(eps2)
    elif step>=step_max:
        stop_cond='4. L_iter > ' +str(step_max)

    det=numpy.linalg.det(hessian(fun,xi))
 
    return xi,step,fun(xi),stop_cond,step_list,fx_list,det

## getting data form file 'function.dat'
def read_data_form_file(name):

    file = open(name, 'r')
    data = file.readlines()

    fcn_name=data[0][:-1]
    function = data[1][:-1]
    x0 = data[2][:-1]

    file.close()

    return function,x0,fcn_name,


## getting data from user
#function = raw_input("Input function: ")



## function definition
# fun = lambda x: x[0]**2+x[1]**2 # min at (0,0)
# fun = lambda x: (1-x[0])**2+100*(x[1]-x[0])**2 # min at (1,1)
# fun = lambda x: (x[0]**2+x[1]-11)**2+(x[0]+x[1]**2-7)**2 # min at (3,2),(-3.7793,-3.2831),(-2.8051,3.1313),(3.5844,-1.8481)
#fun = lambda x: " ".join(response.split())

if __name__ == "__main__":
    for i in range(1,7):
        file_name = '../data/function'+str(i)+'.dat'
        function,x0,fcn_name = read_data_form_file(file_name)
        min,step,value = newton(function,x0)

        print ("\n File Name: ", file_name)
        print ("    Function name: ", fcn_name)
        print ("    Function: ", function)
        print ("    Initial Point: ", x0)

        print ("\n    min",min)
        print ("    step",step)
        print ("    f(x)",value)
