import sys
from PyQt5 import QtCore, QtGui, QtWidgets
import matplotlib
matplotlib.use('Qt5Agg')
from ui.main_window import Ui_MainWindow
import src.newton_optimize_core as noc
from src.newton_optimize_core import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from numpy import arange, sin, pi

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

from pylab import meshgrid,cm,imshow,contour,clabel,colorbar,axis,title,show,flipud

import matplotlib.pyplot as plt
import matplotlib.path as mpath

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, func = None,step_list=None, fx_list =None ,width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(2,1,1, projection="3d")
        self.axes = self.fig.add_subplot(2,1,2)
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        if func != None:
            
            max_x=step_list[0][0]
            min_x=step_list[0][0]

            max_y=step_list[0][1]
            min_y=step_list[0][1]

            max_fx=fx_list[0]
            min_fx=fx_list[0]
            
            for i in fx_list:
                if i>max_fx: max_fx=i
                if i<min_fx: min_fx=i

            for i in step_list:
                if i[0]>max_x: max_x=i[0]
                if i[0]<min_x: min_x=i[0]
                if i[1]>max_y: max_y=i[1]
                if i[1]<min_y: min_y=i[1]

            delta_x=abs(max_x-min_x)
            delta_y=abs(max_y-min_y)
            delta_fx=abs(max_fx-min_fx)
            
            if delta_x>delta_y : delta = delta_x
            else: delta = delta_y
            
            print('delty:',delta_x,delta_y,delta_fx,delta)
            
            


            #First plot
            ax = self.fig.add_subplot(2,1,1, projection='3d')
            #ax = Axes3D(self.fig.add_subplot(2,1,1, projection = "3d"))
            #x = np.arange(-5,5,0.1)
            #y = np.arange(-5,5,0.1)
            x = np.arange(-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0],delta/20)
            y = np.arange(-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1],delta/20)
            X,Y = np.meshgrid(x, y) # grid of point
            XY = X,Y
            def z_func(x):
                return (eval(func))
            Z = z_func(XY) # evaluation of the function on the grid
            surf = ax.plot_surface(X, Y, Z,rstride=1, cstride=1,cmap=cm.bwr,linewidth=0, antialiased=False)
            ax.view_init(elev=90., azim=90)
            ax.set_xbound([-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0]])
            ax.set_ybound([-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1]])
            ax.set_zbound([-1.2*delta_fx+fx_list[-1],1.2*delta+fx_list[-1]])
           # ax.zaxis.set_major_locator(LinearLocator(10))
           # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
            
            ax2 = self.fig.add_subplot(2,1,2, projection='3d')
            #ax = Axes3D(self.fig.add_subplot(2,1,1, projection = "3d"))
            x = np.arange(-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0],delta/100)
            y = np.arange(-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1],delta/100)
       
            
            X,Y = np.meshgrid(x, y) # grid of point
            XY = X,Y
            def z_func(x):
                return (eval(func))
            Z = z_func(XY) # evaluation of the function on the grid
            surf = ax2.plot_surface(X, Y, Z,rstride=1, cstride=1,cmap=cm.bwr,linewidth=0, antialiased=False)

            ax2.zaxis.set_major_locator(LinearLocator(10))
            ax2.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
           # ax2.set_xbound([-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0]])
           # ax2.set_ybound([-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1]])
          #  ax2.set_zbound([-1.2*delta_fx+fx_list[-1],1.2*delta+fx_list[-1]])
           # ax2.view_init(elev=10, azim=45)

            #Second plot
            
##            ax1 = self.fig.add_subplot(2,1,2)
            
            Path=mpath.Path
            
            path_data=[(Path.MOVETO,(step_list[0][0],step_list[0][1]))]
            
            for i in step_list:
                path_data.append((Path.MOVETO,(i[0],i[1])))
            
            codes, verts = zip(*path_data)
            path = mpath.Path(verts, codes)
            
           
            


          #  x=np.arange(-4.4,6.4,1)
           # y=np.arange(-4.4,6.4,1)
##            print('zakres:',-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0])
##            print('delta:',delta);
##            x = np.arange(-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0],delta/2)
##            y = np.arange(-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1],delta/2)
##            X,Y = np.meshgrid(x, y) # grid of point
##            XY = X,Y
            
           # print('XY',X,Y)
            
##            def z_func(x):
##                return (eval(func))
##            Z = z_func(XY) # evaluation of the function on the grid
            ###########################################################3
            #print(XY)
##            ax1.hold(True)
           # ax1.autoscale(enable=True, axis='both')
##            im = ax1.imshow(Z,cmap=cm.bwr,vmin=-1.2*delta_fx+fx_list[-1],vmax=1.2*delta+fx_list[-1])
            #im = ax1.pcolor(flipud(Z))
            #surf = ax1.contour(Z,arange(-1.2*delta_fx+fx_list[-1],1.2*delta_fx+fx_list[-1],0.1),linewidths=2,cmap=cm.bwr)
            #clabel(surf,inline=True,fmt='%1.1f',fontsize=10)
##            ax1.set_xbound([-1.2*delta+step_list[-1][0],1.2*delta+step_list[-1][0]])
##            ax1.set_ybound([-1.2*delta+step_list[-1][1],1.2*delta+step_list[-1][1]])
            #ax1.set_zbound([-1.2*delta_fx+fx_list[-1],1.2*delta+fx_list[-1]])
##            self.fig.colorbar(im)
            
            x, y = zip(*path.vertices)
##            line, = ax1.plot(x, y, 'go-')
            line, = ax.plot(x, y, 'go-')
            #line, = ax2.plot(x, y, 'go-')
##            ax1.grid()
            
           # print(step_list, fx_list)
            #show()# adding the colobar on the right
            
      #######################################################################33
            #surf = ax.plot_surface(X, Y, Z,rstride=1, cstride=1,cmap=cm.RdBu,linewidth=0, antialiased=False)

            #ax.zaxis.set_major_locator(LinearLocator(10))
            #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        # self.compute_initial_figure()


        #
        FigureCanvas.__init__(self, self.fig)
        # self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Expanding,
                QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)




class GUI(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        for i in range(1,7):
            file_name = 'data/function'+str(i)+'.dat'
            function,x0,fcn_name = noc.read_data_form_file(file_name)
            self.ui.Function.addItem(function)
            self.ui.InitialCondition.addItem(x0)


        self.addPlot()


        self.ui.Function.insertItem(0,"")
        self.ui.InitialCondition.insertItem(0,"")
        self.ui.Function.setCurrentIndex(0)
        self.ui.InitialCondition.setCurrentIndex(0)

        self.ui.StartButton.clicked.connect(self.StartNewton)

    def addPlot(self,func=None,step_list=None,fx_list=None):
        self.sc = MyMplCanvas(func,step_list,fx_list)
        self.sc.setMinimumHeight(800)
        self.sc.setMinimumWidth(1000)
        self.ui.plotLayout.addWidget(self.sc)

    def StartNewton(self):
        function = self.ui.Function.currentText()
        x0 = self.ui.InitialCondition.currentText()


        min,step,value,cond,step_list,fx_list,det = noc.newton(function,x0)

        
        self.ui.min.setText('Minimum at point: '+str(min))
        self.ui.step.setText('Step: '+str(step))
        self.ui.fx.setText('Value: '+ str(value))
        self.ui.condition.setText('Stop condition: ' + cond)
        
        if det>0:
            self.ui.info.setText('Found local or global minimum')
            self.ui.please.setText('')
        elif det<0:
            self.ui.info.setText('Hessian is negative definied, solution is a stationary point')
            self.ui.please.setText('Please choose another start points')

        self.ui.plotLayout.removeWidget(self.sc)
        self.addPlot(function,step_list,fx_list)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = GUI()
    myapp.show()
    sys.exit(app.exec_())
